(function () {
    "use strict";

    angular.module("app").factory("ngTableSimpleList", dataFactory);

    dataFactory.$inject = [];

    function dataFactory() {
        return [
            {
                "id": 35,
                "timestamp": "2016-04-15 16:37:40PDT",
                "issuetype": "Improvement",
                "components": "Prototype",
                "assignee": "asheirah",
                "priority": "Major",
                "resolved": 0,
                "inprogress": 1,
                "open": 0,
                "closed": 0
            },
            {
                "id": 36,
                "timestamp": "2016-04-15 16:37:40PDT",
                "issuetype": "Task",
                "components": "Time-series data",
                "assignee": "larabell",
                "priority": "Major",
                "resolved": 2,
                "inprogress": 0,
                "open": 0,
                "closed": 0
            },
            {
                "id": 37,
                "timestamp": "2016-04-15 16:37:40PDT",
                "issuetype": "Task",
                "components": "Event management",
                "assignee": "larabell",
                "priority": "Major",
                "resolved": 0,
                "inprogress": 0,
                "open": 1,
                "closed": 0
            },
            {
                "id": 38,
                "timestamp": "2016-04-15 16:37:40PDT",
                "issuetype": "Task",
                "components": "Regression Tracking",
                "assignee": "larabell",
                "priority": "Major",
                "resolved": 0,
                "inprogress": 0,
                "open": 3,
                "closed": 0
            },
            {
                "id": 39,
                "timestamp": "2016-04-15 16:37:40PDT",
                "issuetype": "Task",
                "components": "Visualization (generic widgets)",
                "assignee": "larabell",
                "priority": "Major",
                "resolved": 0,
                "inprogress": 0,
                "open": 10,
                "closed": 0
            },
            {
                "id": 40,
                "timestamp": "2016-04-15 16:37:40PDT",
                "issuetype": "Task",
                "components": "Database",
                "assignee": "larabell",
                "priority": "Major",
                "resolved": 0,
                "inprogress": 0,
                "open": 1,
                "closed": 0
            },
            {
                "id": 41,
                "timestamp": "2016-04-15 16:37:40PDT",
                "issuetype": "Task",
                "components": "Project/estimation analysis",
                "assignee": "larabell",
                "priority": "Major",
                "resolved": 0,
                "inprogress": 0,
                "open": 2,
                "closed": 0
            },
            {
                "id": 42,
                "timestamp": "2016-04-15 16:37:40PDT",
                "issuetype": "Task",
                "components": "CServer",
                "assignee": "larabell",
                "priority": "Major",
                "resolved": 4,
                "inprogress": 3,
                "open": 16,
                "closed": 0
            },
            {
                "id": 43,
                "timestamp": "2016-04-15 16:37:40PDT",
                "issuetype": "Task",
                "components": "Verification Portal (dashboard)",
                "assignee": "larabell",
                "priority": "Major",
                "resolved": 0,
                "inprogress": 0,
                "open": 2,
                "closed": 0
            },
            {
                "id": 44,
                "timestamp": "2016-04-15 16:37:40PDT",
                "issuetype": "Task",
                "components": "Testplan Management",
                "assignee": "None",
                "priority": "Major",
                "resolved": 0,
                "inprogress": 0,
                "open": 1,
                "closed": 0
            },
            {
                "id": 45,
                "timestamp": "2016-04-15 16:37:40PDT",
                "issuetype": "Task",
                "components": "Development Infrastructure",
                "assignee": "larabell",
                "priority": "Major",
                "resolved": 0,
                "inprogress": 0,
                "open": 2,
                "closed": 0
            },
            {
                "id": 46,
                "timestamp": "2016-04-15 16:37:40PDT",
                "issuetype": "Task",
                "components": "Licensing",
                "assignee": "larabell",
                "priority": "Major",
                "resolved": 0,
                "inprogress": 0,
                "open": 1,
                "closed": 0
            },
            {
                "id": 47,
                "timestamp": "2016-04-15 16:37:40PDT",
                "issuetype": "Task",
                "components": "Coverage/trending",
                "assignee": "larabell",
                "priority": "Major",
                "resolved": 0,
                "inprogress": 0,
                "open": 1,
                "closed": 0
            },
            {
                "id": 48,
                "timestamp": "2016-04-15 16:37:40PDT",
                "issuetype": "Task",
                "components": "Production testbed",
                "assignee": "larabell",
                "priority": "Major",
                "resolved": 0,
                "inprogress": 0,
                "open": 6,
                "closed": 0
            },
            {
                "id": 49,
                "timestamp": "2016-04-15 16:37:40PDT",
                "issuetype": "Task",
                "components": "Prototype",
                "assignee": "darronm",
                "priority": "Major",
                "resolved": 5,
                "inprogress": 2,
                "open": 3,
                "closed": 0
            },
            {
                "id": 50,
                "timestamp": "2016-04-15 16:37:40PDT",
                "issuetype": "Task",
                "components": "Prototype",
                "assignee": "None",
                "priority": "Major",
                "resolved": 0,
                "inprogress": 0,
                "open": 32,
                "closed": 0
            },
            {
                "id": 51,
                "timestamp": "2016-04-15 16:37:40PDT",
                "issuetype": "Task",
                "components": "Prototype",
                "assignee": "slaha",
                "priority": "Major",
                "resolved": 0,
                "inprogress": 1,
                "open": 0,
                "closed": 0
            },
            {
                "id": 52,
                "timestamp": "2016-04-15 16:37:40PDT",
                "issuetype": "Task",
                "components": "Prototype",
                "assignee": "larabell",
                "priority": "Major",
                "resolved": 6,
                "inprogress": 0,
                "open": 10,
                "closed": 0
            },
            {
                "id": 53,
                "timestamp": "2016-04-15 16:37:40PDT",
                "issuetype": "Task",
                "components": "Alerts and messaging",
                "assignee": "larabell",
                "priority": "Major",
                "resolved": 0,
                "inprogress": 0,
                "open": 1,
                "closed": 0
            },
            {
                "id": 34,
                "timestamp": "2016-04-15 16:37:40PDT",
                "issuetype": "Improvement",
                "components": "None",
                "assignee": "None",
                "priority": "Major",
                "resolved": 0,
                "inprogress": 0,
                "open": 0,
                "closed": 2
            }
        ];
    }
})();


(function () {
    "use strict";

    angular.module('app').controller("HomeCtrl", HomeCtrl);

    HomeCtrl.$inject = ["NgTableParams", "ngTableSimpleList"];



    function HomeCtrl(NgTableParams, simpleList) {
        var self = this;
        self.tableParams = new NgTableParams({}, {
            dataset: simpleList
        });
    }
})();


(function () {
    "use strict";

    angular.module('app').run(configureDefaults);
    configureDefaults.$inject = ["ngTableDefaults"];

    function configureDefaults(ngTableDefaults) {
        ngTableDefaults.params.count = 5;
        ngTableDefaults.settings.counts = [];
    }
})();


(function () {
    "use strict";

    angular.module("app").directive("loadingContainer", function () {
        return {
            restrict: "A",
            scope: false,
            link: function (scope, element, attrs) {
                var loadingLayer = angular.element("<div class='loading'></div>");
                element.append(loadingLayer);
                element.addClass("loading-container");
                scope.$watch(attrs.loadingContainer, function (value) {
                    loadingLayer.toggleClass("ng-hide", !value);
                });
            }
        };
    })
})();
