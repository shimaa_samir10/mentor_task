"use strict";
angular.module("app")
    .controller("NestedListsDemoController", function ($scope) {

        $scope.models = {
            selected: null,
            templates: [
                {type: "item", id: 2},
                {type: "container", id: 1, columns: [[], []]}
            ],
            dropzones: {
                "A": [
                    {
                        "type": "open",
                        "id": 1
                    }
                ],
                "B": [

                    {
                        "type": "resolved",
                        "id": "2"
                    },
                    {
                        "type": "progress",
                        "id": 3
                    }
                ]
            }
        };

        $scope.$watch('models.dropzones', function (model) {
            $scope.modelAsJson = angular.toJson(model, true);
        }, true);


        // Open Chart
        $scope.widget_1_options = {
            chart: {
                type: 'pieChart',
                height: 300,
                x: function (d) {
                    return d.name;
                },
                y: function (d) {
                    return d.opened;
                },
                showLabels: true,
                duration: 500,
                labelThreshold: 0.01,
                labelSunbeamLayout: true,
                legend: {
                    margin: {
                        top: 5,
                        right: 35,
                        bottom: 5,
                        left: 0
                    }
                }
            }
        };
        $scope.widget_2_options = {
            chart: {
                type: 'pieChart',
                height: 300,
                donut: true,
                x: function (d) {
                    return d.name;
                },
                y: function (d) {
                    return d.resolved;
                },
                showLabels: true,

                pie: {
                    startAngle: function (d) {
                        return d.startAngle / 2 - Math.PI / 2
                    },
                    endAngle: function (d) {
                        return d.endAngle / 2 - Math.PI / 2
                    }
                },
                duration: 500,
                legend: {
                    margin: {
                        top: 5,
                        right: 70,
                        bottom: 5,
                        left: 0
                    }
                }
            }
        };
        $scope.widget_3_options = {
            chart: {
                type: 'pieChart',
                height: 300,
                x: function (d) {
                    return d.name;
                },
                y: function (d) {
                    return d.prog;
                },
                showLabels: true,
                duration: 500,
                labelThreshold: 0.01,
                labelSunbeamLayout: true,
                legend: {
                    margin: {
                        top: 5,
                        right: 35,
                        bottom: 5,
                        left: 0
                    }
                }
            }
        };


        $scope.data = [
            {
                "id": 35,
                "timestamp": "2016-04-15 16:37:40PDT",
                "issuetype": "Improvement",
                "components": "Prototype",
                "assignee": "asheirah",
                "priority": "Major",
                "resolved": 0,
                "inprogress": 1,
                "open": 0,
                "closed": 0
            },
            {
                "id": 36,
                "timestamp": "2016-04-15 16:37:40PDT",
                "issuetype": "Task",
                "components": "Time-series data",
                "assignee": "larabell",
                "priority": "Major",
                "resolved": 2,
                "inprogress": 0,
                "open": 0,
                "closed": 0
            },
            {
                "id": 37,
                "timestamp": "2016-04-15 16:37:40PDT",
                "issuetype": "Task",
                "components": "Event management",
                "assignee": "larabell",
                "priority": "Major",
                "resolved": 0,
                "inprogress": 0,
                "open": 1,
                "closed": 0
            },
            {
                "id": 38,
                "timestamp": "2016-04-15 16:37:40PDT",
                "issuetype": "Task",
                "components": "Regression Tracking",
                "assignee": "larabell",
                "priority": "Major",
                "resolved": 0,
                "inprogress": 0,
                "open": 3,
                "closed": 0
            },
            {
                "id": 39,
                "timestamp": "2016-04-15 16:37:40PDT",
                "issuetype": "Task",
                "components": "Visualization (generic widgets)",
                "assignee": "larabell",
                "priority": "Major",
                "resolved": 0,
                "inprogress": 0,
                "open": 10,
                "closed": 0
            },
            {
                "id": 40,
                "timestamp": "2016-04-15 16:37:40PDT",
                "issuetype": "Task",
                "components": "Database",
                "assignee": "larabell",
                "priority": "Major",
                "resolved": 0,
                "inprogress": 0,
                "open": 1,
                "closed": 0
            },
            {
                "id": 41,
                "timestamp": "2016-04-15 16:37:40PDT",
                "issuetype": "Task",
                "components": "Project/estimation analysis",
                "assignee": "larabell",
                "priority": "Major",
                "resolved": 0,
                "inprogress": 0,
                "open": 2,
                "closed": 0
            },
            {
                "id": 42,
                "timestamp": "2016-04-15 16:37:40PDT",
                "issuetype": "Task",
                "components": "CServer",
                "assignee": "larabell",
                "priority": "Major",
                "resolved": 4,
                "inprogress": 3,
                "open": 16,
                "closed": 0
            },
            {
                "id": 43,
                "timestamp": "2016-04-15 16:37:40PDT",
                "issuetype": "Task",
                "components": "Verification Portal (dashboard)",
                "assignee": "larabell",
                "priority": "Major",
                "resolved": 0,
                "inprogress": 0,
                "open": 2,
                "closed": 0
            },
            {
                "id": 44,
                "timestamp": "2016-04-15 16:37:40PDT",
                "issuetype": "Task",
                "components": "Testplan Management",
                "assignee": "None",
                "priority": "Major",
                "resolved": 0,
                "inprogress": 0,
                "open": 1,
                "closed": 0
            },
            {
                "id": 45,
                "timestamp": "2016-04-15 16:37:40PDT",
                "issuetype": "Task",
                "components": "Development Infrastructure",
                "assignee": "larabell",
                "priority": "Major",
                "resolved": 0,
                "inprogress": 0,
                "open": 2,
                "closed": 0
            },
            {
                "id": 46,
                "timestamp": "2016-04-15 16:37:40PDT",
                "issuetype": "Task",
                "components": "Licensing",
                "assignee": "larabell",
                "priority": "Major",
                "resolved": 0,
                "inprogress": 0,
                "open": 1,
                "closed": 0
            },
            {
                "id": 47,
                "timestamp": "2016-04-15 16:37:40PDT",
                "issuetype": "Task",
                "components": "Coverage/trending",
                "assignee": "larabell",
                "priority": "Major",
                "resolved": 0,
                "inprogress": 0,
                "open": 1,
                "closed": 0
            },
            {
                "id": 48,
                "timestamp": "2016-04-15 16:37:40PDT",
                "issuetype": "Task",
                "components": "Production testbed",
                "assignee": "larabell",
                "priority": "Major",
                "resolved": 0,
                "inprogress": 0,
                "open": 6,
                "closed": 0
            },
            {
                "id": 49,
                "timestamp": "2016-04-15 16:37:40PDT",
                "issuetype": "Task",
                "components": "Prototype",
                "assignee": "darronm",
                "priority": "Major",
                "resolved": 5,
                "inprogress": 2,
                "open": 3,
                "closed": 0
            },
            {
                "id": 50,
                "timestamp": "2016-04-15 16:37:40PDT",
                "issuetype": "Task",
                "components": "Prototype",
                "assignee": "None",
                "priority": "Major",
                "resolved": 0,
                "inprogress": 0,
                "open": 32,
                "closed": 0
            },
            {
                "id": 51,
                "timestamp": "2016-04-15 16:37:40PDT",
                "issuetype": "Task",
                "components": "Prototype",
                "assignee": "slaha",
                "priority": "Major",
                "resolved": 0,
                "inprogress": 1,
                "open": 0,
                "closed": 0
            },
            {
                "id": 52,
                "timestamp": "2016-04-15 16:37:40PDT",
                "issuetype": "Task",
                "components": "Prototype",
                "assignee": "larabell",
                "priority": "Major",
                "resolved": 6,
                "inprogress": 0,
                "open": 10,
                "closed": 0
            },
            {
                "id": 53,
                "timestamp": "2016-04-15 16:37:40PDT",
                "issuetype": "Task",
                "components": "Alerts and messaging",
                "assignee": "larabell",
                "priority": "Major",
                "resolved": 0,
                "inprogress": 0,
                "open": 1,
                "closed": 0
            },
            {
                "id": 34,
                "timestamp": "2016-04-15 16:37:40PDT",
                "issuetype": "Improvement",
                "components": "None",
                "assignee": "None",
                "priority": "Major",
                "resolved": 0,
                "inprogress": 0,
                "open": 0,
                "closed": 2
            }
        ];
        $scope.mergedData = [];

        for (var i = 0; i < $scope.data.length;) {
            var name = $scope.data[i].assignee;
            var sumOpened = 0;
            var sumResolved = 0;
            var sumProg = 0;
            for (var k = i; k < $scope.data.length; k++) {
                if ($scope.data[k].assignee == name) {
                    sumOpened += $scope.data[k].open;
                    sumResolved += $scope.data[k].resolved;
                    sumProg += $scope.data[k].inprogress;
                    $scope.data.splice(k, 1);
                    k--;
                }
            }

            $scope.mergedDataPerUser = {
                name: name,
                opened: sumOpened,
                resolved: sumResolved,
                prog: sumProg
            }

            $scope.mergedData.push(($scope.mergedDataPerUser))
        }


    });
