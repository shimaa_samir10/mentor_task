/* ============================================================
 * File: config.lazyload.js
 * Configure modules for ocLazyLoader. These are grouped by 
 * vendor libraries. 
 * ============================================================ */

angular.module('app')
    .config(['$ocLazyLoadProvider', function ($ocLazyLoadProvider) {
        $ocLazyLoadProvider.config({
            debug: true,
            events: true,
            modules: [{
                name: 'nvd3',
                files: [
                    'assets/plugins/nvd3/lib/d3.v3.js',
                    'assets/plugins/nvd3/nv.d3.min.js',
                    'assets/plugins/nvd3/src/utils.js',
                    'assets/plugins/nvd3/src/tooltip.js',
                    'assets/plugins/nvd3/src/interactiveLayer.js',
                    'assets/plugins/nvd3/src/models/axis.js',
                    'assets/plugins/nvd3/src/models/line.js',
                    'assets/plugins/nvd3/src/models/lineWithFocusChart.js',
                    'assets/plugins/angular-nvd3/angular-nvd3.js',
                    'assets/plugins/nvd3/nv.d3.min.css'
                ],
                serie: true // load in the exact order
            }, {
                name: 'dnd',
                files: [
                    'assets/plugins/angular-drag-and-drop-lists.min.js'
                ]
            },
                {
                    name: 'ngTable',
                    files: [
                        'assets/plugins/ng-table/bundles/ng-table.js',
                        'assets/plugins/ng-table/bundles/ng-table.css'
                    ]
                }

            ]
        });
    }]);